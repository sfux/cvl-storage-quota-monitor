#!/bin/bash

########################################################################################
# Script to parse the quota report in /cluster/work/cvl/.cvl_User_Usage.txt and send   #
# alerts in case some limits are exceeded                                              #
#                                                                                      #
# author: Samuel Fux                                                                   #
# 2021 @ETH Zurich                                                                     #
########################################################################################

##########################################################
## general options: initialize variables with a default ##
##########################################################

# uncomment next line for debugging mode of the script
#set -x

# textfile containing the quota report that is updated 4 times per day
QUOTA_REPORT_FILE=/cluster/work/cvl/.cvl_User_Usage.txt

# logfile
QUOTA_LOG_FILE=/cluster/work/cvl/.quota.log

# limits that cause an alert to be sent
# absolute size in percent
QUOTA_ABSOLUTE_LIMIT=90
# relative growth in percent between two quota reports (currently this corresponds to a time span of 6 hours)
QUOTA_GROWTH_LIMIT=5

# email address to which the alert is sent
QUOTA_ALERT_EMAIL_ADDRESS="samuel.fux@id.ethz.ch"

# Variable to control verbosity of script. By default the script is silent and only prints errors/warnings
VERBOSE=0

##########################
# end of general options #
##########################

# variables for quota checks
SPACE_QUOTA_OVERFLOW=0
INODE_QUOTA_OVERFLOW=0
SPACE_QUOTA_GROWTH_OVERFLOW=0
INODE_QUOTA_GROWTH_OVERFLOW=0

## help message and parsing command line arguments

# function to display help message
display_help()
{
cat <<-EOF
$0: Command to parse the daily quota report in /cluster/work/cvl/.cvl_User_Usage.txt

Usage: quota_monitor.sh [-h] [-v]

Options:

        -h | --help                         Display this help message and exit
        -v | --verbose                      Versbose mode (otherwise only errors/warnings are printed)

Example:

        quota_monitor.sh

EOF
exit 1
}

# if there are some command line arguments parse them and set necessary variables
while [[ $# -gt 0 ]]
do
        case $1 in
                -h|--help)
                shift
                display_help
                ;;
                -v|--verbose)
                VERBOSE=1
                shift
                ;;
                *)
                echo -e "Warning: ignoring unknown option $1 \n"
                shift
                ;;
        esac
done


## parsing quota report and logfile if one exists and send an alarm to $QUOTA_ALERT_EMAIL_ADDRESS

# check if quota report file exists and exit if it does not
if [ ! -s "$QUOTA_REPORT_FILE" ]; then
        echo -e "Error: quota report file $QUOTA_REPORT_FILE does not exist, or is empty"
        exit 1
fi

# the following command parses the quota report file and extracts two numbers (% space used, % inodes used) from the last 2 lines
# ${CURRENT_QUOTA[0]} -> percentage of space quota used
# ${CURRENT_QUOTA[1]} -> percentage of inode quota used
CURRENT_QUOTA=( $(tail -n 2 ${QUOTA_REPORT_FILE} | grep -o '[^ ]*%' | cut -d "%" -f 1) )

# check if there are numbers available from the previous quota check
GROWTH_CHECK=0
if [ -s "$QUOTA_LOG_FILE" ]; then
        GROWTH_CHECK=1
        # if the file is not empty, it has at least 1 line, so we can get the numbers from the previous quota report from the last line
        # ${LAST_QUOTA[0]} -> percentage of space quota used
        # ${LAST_QUOTA[1]} -> percentage of inode quota used
        LAST_QUOTA=( $(tail -n 1 ${QUOTA_LOG_FILE} | grep -o '[^ ]*%' | cut -d "%" -f 1) )
else
        echo -e "Warning: no old quota numbers available. Either logfile $QUOTA_LOG_FILE does not exist or is empty. Only checking absolute quota usage, not relative growth"
        GROWTH_CHECK=0
fi

# write actual quota usage to $QUOTA_LOG_FILE to save its values for comparison with the numbers from the next future quota report
CURRENT_DATE=$(date '+%d/%m/%Y %H:%M:%S');
echo -e "Date: $CURRENT_DATE; space quota used: ${CURRENT_QUOTA[0]}%; inode quota used: ${CURRENT_QUOTA[1]}%;" >> $QUOTA_LOG_FILE

# check if quotas are above $QUOTA_ABSOLUTE_LIMIT
SPACE_QUOTA_OVERFLOW=$(bc -l <<< "${CURRENT_QUOTA[0]} >= ${QUOTA_ABSOLUTE_LIMIT}")
INODE_QUOTA_OVERFLOW=$(bc -l <<< "${CURRENT_QUOTA[1]} >= ${QUOTA_ABSOLUTE_LIMIT}")


# check if growth of quota is above $QUOTA_GROWTH_LIMIT
if [ "$GROWTH_CHECK" -eq "1" ]; then
        SPACE_QUOTA_GROWTH=$(bc -l <<< "${CURRENT_QUOTA[0]} - ${LAST_QUOTA[0]}")
        INODE_QUOTA_GROWTH=$(bc -l <<< "${CURRENT_QUOTA[1]} - ${LAST_QUOTA[1]}")
        SPACE_QUOTA_GROWTH_OVERFLOW=$(bc -l <<< "${SPACE_QUOTA_GROWTH} >= ${QUOTA_GROWTH_LIMIT}")
        INODE_QUOTA_GROWTH_OVERFLOW=$(bc -l <<< "${INODE_QUOTA_GROWTH} >= ${QUOTA_GROWTH_LIMIT}")
fi

# if any of the overflow variables is set to 1 (true), then an alarm is sent to $QUOTA_ALERT_EMAIL_ADDRESS
if [ "$SPACE_QUOTA_OVERFLOW" = "1" ] || [ "$INODE_QUOTA_OVERFLOW" = "1" ] || [ "$SPACE_QUOTA_GROWTH_OVERFLOW" = "1" ] || [ "$INODE_QUOTA_GROWTH_OVERFLOW" = "1" ]; then
# send alert email to $QUOTA_ALERT_EMAIL_ADDRESS

# write temporary file containing the text of the email
cat <<EOF > $HOME/quota_email
From: Cluster Support [cluster-support@id.ethz.ch](mailto:cluster-support@id.ethz.ch)
Reply-To: Cluster Support [cluster-support@id.ethz.ch](mailto:cluster-support@id.ethz.ch)
To: $QUOTA_ALERT_EMAIL_ADDRESS
Subject: Quota alert for /cluster/work/cvl
Content-Type: text/html; charset="utf-8"
MIME-Version: 1.0

<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<body>
<p>Hi $USER,</p>
<p>This is a quota alert for the storage location /cluster/work/cvl.</p>

EOF
bash <<EOF >> $HOME/quota_email
if [ "$SPACE_QUOTA_OVERFLOW" = "1" ]; then
echo -e "<p>Space quota ${CURRENT_QUOTA[0]}% exeeded the limit of ${QUOTA_ABSOLUTE_LIMIT}%</p>"
fi
if [ "$INODE_QUOTA_OVERFLOW" = "1" ]; then
echo -e "<p>Inode quota ${CURRENT_QUOTA[1]}% exeeded the limit of ${QUOTA_ABSOLUTE_LIMIT}%</p>"
fi
if [ "$SPACE_QUOTA_GROWTH_OVERFLOW" = "1" ]; then
echo -e "<p>Space quota growth (within last 6 hours) ${SPACE_QUOTA_GROWTH}% exeeded the limit of ${QUOTA_GROWTH_LIMIT}%</p>"
fi
if [ "$INODE_QUOTA_GROWTH_OVERFLOW" = "1" ]; then
echo -e "<p>Inode quota growth (within last 6 hours) ${INODE_QUOTA_GROWTH}% exeeded the limit of ${QUOTA_GROWTH_LIMIT}%</p>"
fi
EOF
cat <<EOF >> $HOME/quota_email
<p>With kind regards,<br />
Your cluster support team.</p>

<p>Current quota report file:</p>
<pre style="font: monospace">
EOF
cat $QUOTA_REPORT_FILE >> $HOME/quota_email
cat <<EOF >> $HOME/quota_email
</pre>
</body>
</html>
EOF
# send email
sendmail -t -bm -f cluster-support@id.ethz.ch < $HOME/quota_email
# remove temporary file with the email text
if [ -f $HOME/quota_email ]; then
        rm $HOME/quota_email
fi
fi
