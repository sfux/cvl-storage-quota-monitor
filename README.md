# CVL Storage Quota Monitor

The script quota_monitor.sh is used to parse the daily quota report for the /cluster/work/cvl storage on Euler

Since the quota report is replaced 4 times per day with a new version, the script writes a logfile .quota.log, where the percentage of the space and inode quota is written down. This allows to look at the growth of the quota usage with respect to the last quota report file.

The configuration parameters for this script (set at in the top part of the script) are

```
QUOTA_REPORT_FILE=/cluster/work/cvl/.cvl_User_Usage.txt
QUOTA_LOG_FILE=/cluster/work/cvl/.quota.log
QUOTA_ABSOLUTE_LIMIT=90
QUOTA_GROWTH_LIMIT=5
QUOTA_ALERT_EMAIL_ADDRESS="samuel.fux@id.ethz.ch"
```

`QUOTA_REPORT_FILE         :` file containing the quota report provided by the storage admin  
`QUOTA_LOG_FILE            :` logfile containing the percentage of space and inode quota from previous quota reports, used to track the usage over time  
`QUOTA_ABSOLUTE_LIMIT      :` quota usage (space or inodes) in percent that triggers an email alert to be sent  
`QUOTA_GROWTH_LIMIT        :` change in quota usage (space or inodes) with respect to the last quota report in percent that triggers an email alert to be sent  
`QUOTA_ALERT_EMAIL_ADDRESS :` email address to which the alert is sent  

The email alert informs which quota limit is reached (space or inode) and if the absolute quota limit or the quota growth limit has been exceeded. At the end of the email, the current quota report file is included.

Example alert email:

```
Von: Cluster@eu-login-09.euler.ethz.ch <Cluster@eu-login-09.euler.ethz.ch>
Gesendet: Donnerstag, 25. Februar 2021 11:26
An: Fux Samuel (ID SIS)
Betreff: Quota alert for /cluster/work/cvl
 

Hi sfux,

This is a quota alert for the storage location /cluster/work/cvl.

Space quota growth (within last 6 hours) 9.0% exeeded the limit of 5%

With kind regards,
Your cluster support team.

Current quota report file:

Usage report as of Mon, 22 Feb 2021 06:07:01 on /cluster/work/cvl 

Number of users allowed to write to /cluster/work/cvl (according to group ITET-BIWI-hpc): 93

User space usage:
       mhahner space used:     6645.54 GB, 	file usage:           4002
    tobfischer space used:     2798.09 GB, 	file usage:         451136
      jonatank space used:     1838.01 GB, 	file usage:           8780
        voanna space used:     1520.11 GB, 	file usage:          10074
       truongp space used:     1295.11 GB, 	file usage:         436954
      alugmayr space used:     1007.46 GB, 	file usage:           3436
       tiazhou space used:      761.60 GB, 	file usage:         717574
   esandstroem space used:      742.59 GB, 	file usage:         890666
      xiaxiali space used:      650.71 GB, 	file usage:           4758
         yawli space used:      602.73 GB, 	file usage:        2666355
       gusingh space used:      488.46 GB, 	file usage:         118833
       jiezcao space used:      381.59 GB, 	file usage:         165451
        ouenal space used:      326.32 GB, 	file usage:            766
       chmayer space used:      313.02 GB, 	file usage:          16945
      jinliang space used:      249.03 GB, 	file usage:          93099
        zzhiwu space used:      210.91 GB, 	file usage:         356042
          cany space used:      196.74 GB, 	file usage:        1109797
      roandres space used:      172.22 GB, 	file usage:          17357
      kaizhang space used:      166.76 GB, 	file usage:          67146
         qwang space used:      150.42 GB, 	file usage:          57363
         timbr space used:      146.38 GB, 	file usage:              7
        dfuoli space used:      120.45 GB, 	file usage:            172
         bhatg space used:      116.90 GB, 	file usage:              6
     nipopovic space used:       58.54 GB, 	file usage:            172
        guosun space used:       41.02 GB, 	file usage:         123304
        zaechj space used:       36.29 GB, 	file usage:           1025
      jpostels space used:       17.04 GB, 	file usage:            184
        bekaya space used:       15.53 GB, 	file usage:          17671
        menliu space used:       14.22 GB, 	file usage:              2
      kanakism space used:        7.32 GB, 	file usage:           2635
       brdavid space used:        5.26 GB, 	file usage:            166
        himeva space used:        5.05 GB, 	file usage:          70577
      zhejzhan space used:        0.00 GB, 	file usage:              1
        reyang space used:        0.00 GB, 	file usage:              1
       probstt space used:        0.00 GB, 	file usage:              1
      obukhova space used:        0.00 GB, 	file usage:              1
       nkarani space used:        0.00 GB, 	file usage:              1
     mshahbazi space used:        0.00 GB, 	file usage:             57
       linzhan space used:        0.00 GB, 	file usage:              1
      krishnch space used:        0.00 GB, 	file usage:              1
      aliniger space used:        0.00 GB, 	file usage:              1

Warning: only users currently in LDAP group ITET-BIWI-hpc can be taken into statistics. Other (old and from group removed) users are NOT counted here!
Warning: users belonging to several working groups might give incorrect results as all files in all groups are accounted for these stats


Volume usage:

space used:       21.61 TB, quota:              198 TB, usage: 10.9%
file usage:        7412530, file quota:       49500000, usage: 15.0%
```


